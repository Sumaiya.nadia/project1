<?php

namespace App\Http\Controllers;

use App\awards_and_achievements;
use Illuminate\Http\Request;

class AwardsAndAchievementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\awards_and_achievements  $awards_and_achievements
     * @return \Illuminate\Http\Response
     */
    public function show(awards_and_achievements $awards_and_achievements)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\awards_and_achievements  $awards_and_achievements
     * @return \Illuminate\Http\Response
     */
    public function edit(awards_and_achievements $awards_and_achievements)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\awards_and_achievements  $awards_and_achievements
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, awards_and_achievements $awards_and_achievements)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\awards_and_achievements  $awards_and_achievements
     * @return \Illuminate\Http\Response
     */
    public function destroy(awards_and_achievements $awards_and_achievements)
    {
        //
    }
}
