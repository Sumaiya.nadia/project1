<?php

namespace App\Http\Controllers;

use App\hire_links;
use Illuminate\Http\Request;

class HireLinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\hire_links  $hire_links
     * @return \Illuminate\Http\Response
     */
    public function show(hire_links $hire_links)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\hire_links  $hire_links
     * @return \Illuminate\Http\Response
     */
    public function edit(hire_links $hire_links)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\hire_links  $hire_links
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, hire_links $hire_links)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\hire_links  $hire_links
     * @return \Illuminate\Http\Response
     */
    public function destroy(hire_links $hire_links)
    {
        //
    }
}
