<?php

namespace App\Http\Controllers;

use App\trainings;
use Illuminate\Http\Request;

class TrainingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function show(trainings $trainings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function edit(trainings $trainings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, trainings $trainings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function destroy(trainings $trainings)
    {
        //
    }
}
