<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function updateUser($request, $id){
        $user =  DB::table('users')->where('id', $id)->first();
        if($request->name){
            $update = DB::table('users')->where('id', $id)->update(['name' => $request->name ]);
            if ($update)
                return 1;
            else return 0;
        }
        else if($request->email){
            $update = DB::table('users')->where('id', $id)->update(['email' => $request->email ]);
            if ($update)
                return 1;
            else return 0;
        }
        else if($request->phone){
            $update = DB::table('users')->where('id', $id)->update(['phone' => $request->phone ]);
            if ($update)
                return 1;
            else return 0;
        }
        else if($request->dob){
            $update = DB::table('users')->where('id', $id)->update(['date_of_birth' => $request->dob ]);
            if ($update)
                return 1;
            else return 0;
        }
        if($request->address){
            $update = DB::table('users')->where('id', $id)->update(['address' => $request->address ]);
            if ($update)
                return 1;
            else return 0;
        }
    }
    


    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array   */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    
}