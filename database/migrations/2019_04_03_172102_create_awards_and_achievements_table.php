<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardsAndAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awards_and_achievements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('awards');
            $table->integer('projects_done');
            $table->integer('happy_client');
            $table->integer('cups_of_coffee');
            $table->timestamps();
        });
        Schema::table('awards_and_achievements', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awards_and_achievements');
    }
}
