@extends('layouts.admin')

@section('content')


<!-- The Modal -->
  <div class="modal" id="editNameModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Name</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="POST" action="{{ route('user.update', Auth::user()->id) }}">
  		  	@csrf
  		  	<input type="text" name="name" value="{{$user->name}}" required>
  		  	<button type="submit" class="btn btn-info">Update<button>
  		  </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Modal end -->

<!-- The Modal -->
  <div class="modal" id="editEmailModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Email</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="POST" action="{{ route('user.update', Auth::user()->id) }}">
  		  	@csrf
  		  	<input type="email" name="email" value="{{$user->email}}" required>
  		  	<button type="submit" class="btn btn-info">Update<button>
  		  </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Modal end -->

<!-- The Modal -->
  <div class="modal" id="editPhoneModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Phone</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="POST" action="{{ route('user.update', Auth::user()->id) }}">
  		  	@csrf
  		  	<input type="number" name="phone" value="{{$user->phone}}" required>
  		  	<button type="submit" class="btn btn-info">Update<button>
  		  </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Modal end -->

<!-- The Modal -->
  <div class="modal" id="editDobModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Date of Birth</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="POST" action="{{ route('user.update', Auth::user()->id) }}">
  		  	@csrf
  		  	<input type="date" name="dob" value="{{$user->date_of_birth}}" required>
  		  	<button type="submit" class="btn btn-info">Update<button>
  		  </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Modal end -->

<!-- The Modal -->
  <div class="modal" id="editAddressModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Address</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="POST" action="{{ route('user.update', Auth::user()->id) }}">
  		  	@csrf
  		  	<input type="text" name="address" value="{{$user->address}}" required>
  		  	<button type="submit" class="btn btn-info">Update<button>
  		  </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Modal end -->




<div class="container">
<div class="row">
<div class="col-md-12">

	<!-- flash message -->
    <div class="row">
    	<div class="col-md-6">
			<div class="flash-message">
				@if(Session::has('message'))
    			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    			@endif
    		</div>
		</div>
   		<div class="col-md-6"></div>
	</div>
	<!-- flash message end -->
  
  	<table class="table table-hover">
  		<tr class="form-group">
  		   	<td>Name</td>
  			<td>{{ $user->name }} </td>
  			<td>  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editNameModal">Edit</button></td>
  		</tr>
  		<tr>
  			<td>Email</td>
  			<td>{{ $user->email }}</td>
  			<td>  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editEmailModal">Edit</button></td>
  		</tr>
  		<tr>
  			<td>Phone</td>
  			<td>{{ $user->phone }}</td>
  			<td>  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editPhoneModal">Edit</button></td>
  		</tr>
  		<tr>
  			<td>Date of Birth</td>
  			<td>{{ $user->date_of_birth }}</td>
  			<td>  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editDobModal">Edit</button></td>
  		</tr>
  		<tr>
  			<td>Address</td>
  			<td>{{ $user->address }}</td>
  			<td>  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editAddressModal">Edit</button></td>
  		</tr>
  	</table>

<hr>
</div>
</div>
</div>


@endsection