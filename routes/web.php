<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'UsersController@index')->name('home');


///////
//Route::resource('users', 'UsersController');
Route::get('/user/edit/{id}', 'UsersController@edit')->name('user.edit');
Route::post('/user/update/{id}', 'UsersController@update')->name('user.update');
